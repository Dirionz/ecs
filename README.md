# ecs - Encrypted Cloud Sync


## Links
[https://raymii.org/s/tutorials/Encrypt_and_decrypt_files_to_public_keys_via_the_OpenSSL_Command_Line.html](https://raymii.org/s/tutorials/Encrypt_and_decrypt_files_to_public_keys_via_the_OpenSSL_Command_Line.html)
[https://rietta.com/blog/2012/01/27/openssl-generating-rsa-key-from-command/](https://rietta.com/blog/2012/01/27/openssl-generating-rsa-key-from-command/)
[https://gist.github.com/atoponce/07d8d4c833873be2f68c34f9afc5a78a](https://gist.github.com/atoponce/07d8d4c833873be2f68c34f9afc5a78a)
[https://godoc.org/github.com/benburkert/lockbox](https://godoc.org/github.com/benburkert/lockbox)
[https://jameshfisher.com/2017/04/14/openssl-ecc](https://jameshfisher.com/2017/04/14/openssl-ecc)
[https://www.reddit.com/r/rust/comments/3qvmma/encrypting_file_with_asymmetric_encryption/](https://www.reddit.com/r/rust/comments/3qvmma/encrypting_file_with_asymmetric_encryption/)