#!/usr/bin/env node
'use strict'
process.env.NODE_ENV = 'debug';

const commander = require('./tools/commander.js')
const fs = require('fs');
const yaml = require('js-yaml');
var path = require("path")
var md5 = require('js-md5');

var configFilePath=process.env.HOME+'/.config/ecs/config.yml';

// ./src/ecs.js backup -b test/test-folder -f folder -pk test/publickey.pem -t test/test-folder
// ./src/ecs.js restore -b test/test-folder -f folder -pk test/privatekey.pem -t test/test-folder

var help_str = `
    Usage:
        ecs <command> [options]

    Commands:
        generate    Generate privatekey.pem and publickey.pem
                    -t target directory

        backup      Create encrypted backup
                    -b backup parent directory
                    -f backup directory/filename
                    -t target directory
                    -pk path to publickey.pem
        
        restore     Restore encrypted backup
                    -b backed up parent directory
                    -f backed up directory/filename 
                    -t restore target directory
                    -pk path to privatekey.pem

        sync        Start sync backup of directories in config file
`

if (!process.argv.slice(2).length) {
    console.log(help_str)
} else {
    let commandStr = process.argv[2]
    console.log(commandStr)
    switch (commandStr) {
        case '--version':
            console.log(require('../package.json').version)
            process.exitCode = 0
            break
        case '--help':
            console.log(help_str)
            break
        case 'generate': {
            var targetDir
            let args = process.argv.slice(3, process.argv.length)
            for (let arg of args.slice()) {
                switch(arg) {
                    case '-t':
                        args.shift()
                        targetDir = args[0];
                        args.shift()
                        break;
                }
            }

            generateKeys(targetDir)
                .then(() => {
                    process.exitCode = 0
            }).catch(error => {
                console.log(error)
                process.exitCode = 1
            })
            break
        }
        case 'backup': {
            var backupDir
            var backupFolderName
            let publickey
            let targetDir
            let args = process.argv.slice(3, process.argv.length)
            for (let arg of args.slice()) {
                switch(arg) {
                    case '-b':
                        args.shift()
                        backupDir = args[0];
                        args.shift()
                        break;
                    case '-f':
                        args.shift()
                        backupFolderName = args[0];
                        args.shift()
                        break;
                    case '-t':
                        args.shift()
                        publickey = args[0];
                        args.shift()
                        break;
                    case '-pk':
                        args.shift()
                        targetDir = args[0];
                        args.shift()
                        break;  
                }
            }

            makeBackup(backupDir, backupFolderName, publickey, targetDir)
                .then(() => {
                    process.exitCode = 0
            }).catch(error => {
                console.log(error)
                process.exitCode = 1
            })
            break
        }
        case 'restore': {
            var backedupDir
            var backedupFolderName
            var privatekey
            var targetDir
            let args = process.argv.slice(3, process.argv.length)
            for (let arg of args.slice()) {
                switch(arg) {
                    case '-b':
                        args.shift()
                        backedupDir = args[0];
                        args.shift()
                        break;
                    case '-f':
                        args.shift()
                        backedupFolderName = args[0];
                        args.shift()
                        break;
                    case '-t':
                        args.shift()
                        targetDir = args[0];
                        args.shift()
                        break;
                    case '-pk':
                        args.shift()
                        privatekey = args[0];
                        args.shift()
                        break;  
                }
            }

            restoreBackup(backedupDir, backedupFolderName, privatekey, targetDir)
                .then(() => {
                    process.exitCode = 0
            }).catch(error => {
                console.log(error)
                process.exitCode = 1
            })
            break
        }
        case 'sync': {
            //configFilePath = path.resolve(__dirname) + '/config/config.yml'
            var data = yaml.safeLoad(fs.readFileSync(configFilePath, 'utf8'));

            sync(2000, data)

            break
        }
        default:        
            break
    }
}

async function sync(interval, data) {
    setTimeout(async () => {
        console.log('Sync check');
        let publickey = data['publickey'].replace('~', process.env.HOME);
        let targetDir = data['targetdir'].replace('~', process.env.HOME);

        let directoriesToSync = data['directories'].filter(directory => needsBackup(directory, targetDir))

        for (let directory of directoriesToSync.slice()) {
            console.log('Directory needs sync: ' + directory)
            directory = directory.replace('~', process.env.HOME);
            let backupDir = directory.substring(0, directory.lastIndexOf("/"));
            let backupFolderName = directory.substring(directory.lastIndexOf("/") + 1, directory.length);
            // Check for duplicate backupFolderName in directoriesToSync!
            await makeBackup(backupDir, backupFolderName, publickey, targetDir)
        }

        sync(interval, data);
    }, interval)
}

function needsBackup(directory, targetDir) {
    directory = directory.replace('~', process.env.HOME);
    let backupFolderName = directory.substring(directory.lastIndexOf("/") + 1, directory.length);
    let backedUpPath = targetDir + '/' +  md5(backupFolderName);
    var backedUpMTime = 0
    if (fs.existsSync(backedUpPath)) {
        let backedUpStats = fs.statSync(backedUpPath)
        if (backedUpStats)
            backedUpMTime = backedUpStats.mtimeMs
    }

    var needBackup = false
    var outlist = walkSync(directory)
    for (let file of outlist) { 
        let stats = fs.statSync(file)

        if (stats.mtimeMs > backedUpMTime) {
            needBackup = true
            break
        }
    }

    return needBackup;
}

function walkSync(dir, filelist) {
    var path = path || require('path');
    var fs = fs || require('fs'),
        files = fs.readdirSync(dir);
    filelist = filelist || [];
    files.forEach(function(file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), filelist);
        }
        else {
            filelist.push(path.join(dir, file));
        }
    });
    return filelist;
};

async function generateKeys(targetDir) {
    return new Promise(async resolve => {

        if (!(targetDir))
            throw new Error("Parameters not correct.")

        let pluginsdir = path.resolve(__dirname) + '/scripts'

        let cmd = 'bash ' 
            + pluginsdir 
            + '/gek -t '
            + targetDir

        const res = await commander.cmd(cmd)

        resolve()
    })
}

async function makeBackup(backupDir, backupFolderName, publickey, targetDir) {
    return new Promise(async resolve => {

        if (!(backupDir && backupFolderName && publickey && targetDir))
            throw new Error("Parameters not correct.")

        let pluginsdir = path.resolve(__dirname) + '/scripts'

        let cmd = 'bash ' 
            + pluginsdir 
            + '/meb -b '
            + backupDir
            +' -f '
            + backupFolderName 
            +' -pk '
            + publickey 
            + ' -t ' 
            + targetDir

        console.log(cmd)
        const res = await commander.cmd(cmd)

        resolve()
    })
}

async function restoreBackup(backedupDir, backedupFolderName, privatekey, targetDir) {
    return new Promise(async resolve => {

        if (!(backedupDir && backedupFolderName && privatekey && targetDir))
            throw new Error("Parameters not correct.")

        let pluginsdir = path.resolve(__dirname) + '/scripts'

        let cmd = 'bash ' 
            + pluginsdir 
            + '/reb -b '
            + backedupDir
            +' -f '
            + backedupFolderName 
            +' -pk '
            + privatekey 
            + ' -t ' 
            + targetDir

        const res = await commander.cmd(cmd)

        resolve()
    })
}

process.on('unhandledRejection', function(reason, promise) {
    console.log(reason)
    process.exitCode = 1
});

process.on('SIGINT', function() {
    console.log( "\nGracefully shutting down from SIGINT (Ctrl-C)" );
    process.exit(1);
  });